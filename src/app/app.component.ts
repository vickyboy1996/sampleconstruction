import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'sample-angular';
  product = [{ id: 1, image: "../../assets/image/download.jpeg" }, { id: 2, image: "../../assets/image/green-house.webp" }]
  sampleData: object = {
    title: 'red'
  }
  sample() {
    alert("hi")
  }
  onNotify(value: any) {
    alert(JSON.stringify(value))
  }
}
